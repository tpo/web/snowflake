# Snowflake Website

This repository contains the code for the new Snowflake Website. The current online version of this portal can be accessed at https://Snowflake.torproject.org.

[![pipeline status](https://gitlab.torproject.org/tpo/web/snowflake/badges/main/pipeline.svg)](https://gitlab.torproject.org/tpo/web/snowflake/-/commits/main)



To clone the code

`git clone https://gitlab.torproject.org/tpo/web/snowflake.git`

or browse it [online](https://gitlab.torproject.org/tpo/web/snowflake).

This website is built using HTML, CSS, and Bootstrap 5.3.0 V, seamlessly integrated with Lektor, a powerful CMS for generating and serving websites.

## What is Lektor

[Lektor](https://www.getlektor.com/) is a framework used to generate and serve websites from Markdown files. It provides an efficient way to manage and build websites, making it easier to create and maintain web content.

The source code for Lektor can be found on [GitHub](https://github.com/lektor/lektor).

### Compiling a local version of this website

1. Download and install Lektor: https://www.getlektor.com/downloads/

2. Clone the repository:

         `git clone https://gitlab.torproject.org/tpo/web/snowflake.git`

3. Set up a virtual Python environment

        python3.8 -m venv venv
        source venv/bin/activate
        pip install --upgrade pip lektor

    You can deactivate the virtual environment at any time by running deactivate.



4. Install the dependencies for the lektor-i18n-plugin

    lektor-i18n-plugin requires gettext and python3-babel.
        You can do this by running the following commands:

        sudo apt-get install gettext python3-babel
        pip install babel




5. Build & Serve

        To run a local continuous builder: `$ lektor server`

        To just build the website once: `$ lektor build -O <folder>`


If you encounter any errors during the installation or building process, please refer to the [most common Lektor errors](https://gitlab.torproject.org/tpo/web/team/-/wikis/documentation/Compiling-a-local-version-of-the-website#most-common-lektor-errors) section of our [documentation](https://gitlab.torproject.org/tpo/web/team/-/wikis/documentation/Compiling-a-local-version-of-the-website).
