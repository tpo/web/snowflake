_model: faqs
---
faqs: Frequently asked questions
---
faqsSubText: We’ve compiled answers to some of our most frequently asked questions. If you need additional assistance or more personalized support, please don’t hesitate to contact us through one of our support channels.

---
btnSupportText: Get Support
---

faqQ1: What’s Snowflake?
---
faqQ1Ans: Snowflake allows you to connect to the Tor network in places where Tor is blocked by routing your connection through volunteer proxies located in uncensored countries.
            <br>
            <br>
            Similar to VPNs, which help users bypass Internet censorship, Snowflake disguises your Internet activity as though you’re making a video or voice call, making you less detectable to Internet censors.

---

faqQ2: How does Snowflake work?
---
faqQ2Ans: Snowflake uses a technology called WebRTC, which is commonly employed by videoconferencing software. This helps mask your use of Tor from censors by making it appear as though you’re on a audio or video call instead.
        <br>
        <br>
        Snowflake is a relatively new circumvention technology, part of the Pluggable Transports family, that is continuously being improved. Pluggable Transports disguise a Tor bridges’ traffic by making it look like a regular connection rather than a Tor connection, adding another layer of obfuscation.
        <br>
        <br>
        The disguise is intended to “deceive” censors by making Internet traffic appear as ordinary as a videocall (Snowflake), a connection to Microsoft (meek-azure), a standard HTTPS connect (WebTunnel). It therefore becomes costly for censors to consider blocking such circumvention tools since it would require blocking large parts of the Internet in order to achieve the initial targeted goal.
        <br>
        <br>
        Curious to learn more about its architecture? Feel free to check this [technical overview](https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/wikis/Technical%20Overview) (in English). If you're interested in making use of Snowflake inside your application, get in touch with [anti-censorship team](https://lists.torproject.org/cgi-bin/mailman/listinfo/anti-censorship-team).

---
faqQ3: Can I circumvent censorship using the browser addon?
---
faqQ3Ans: If you would like to circumvent censorship, then you will need to download a Tor-powered app such as Tor Browser or Orbot. If the app is not connecting and it appears that the connection is still blocked, you should be able to unblock the connection by going to the app’s settings and enabling your connection to run through Snowflake.
        <br>
        <br>
        The browser add-on is designed for users who want to support others in circumventing internet censorship. By using Tor-powered apps and enabling Snowflake, your connection will be routed through an add-on installed by a volunteer.
        <br>
        <br>
        Rest assured, volunteers will not be able to determine your location or any other details about your connection while using their add-on.
        
---
_template: faqs.html
---
_hidden: no
---
_discoverable: yes
